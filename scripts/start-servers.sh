#!/bin/bash
killall node
cd server/

npm install
echo 'Server dependencies are installed'

node server.js
echo 'Server is running'

cd ../client/
npm install
bower install
echo 'Client dependencies are installed'

grunt server
echo 'Client server is running' 