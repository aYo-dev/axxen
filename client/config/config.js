'use strict';

angular.module('config', []).constant('APP_CONFIG', {
  serviceBaseUrl: '@@serviceBaseUrl',
  googleAnalyticsKey: '@@googleAnalyticsKey'
});

