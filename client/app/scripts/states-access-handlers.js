'use strict';

angular.module('axxen').run(function ($rootScope, $urlRouter, $injector, $state, authManager, accessExpressionEvaluator) {
  authManager.registerObserverCallback('syncUrlRouter', function () {
    $urlRouter.sync();
  }, true);

  $rootScope.$on('$stateChangeStart', function (event, state, params) {
    if (!authManager.isAuthenticationStateSynced()) {
      event.preventDefault();
      authManager.reloadAuthenticatedUser();
      return;
    }
    
    $rootScope.header = ['admin', 'main'].indexOf(state.name) > -1 ? state.name : undefined;

    var isAvailable = !state.data || !state.data.isAvailable || accessExpressionEvaluator(state.data.isAvailable);
    if (!isAvailable) {
      event.preventDefault();

      if (authManager.getIsUserLogged()) {
        var homeState = authManager.getHomeState();
        $state.go(homeState.name, homeState.params);
      } else {
        authManager.setRequestedStateData({
          name: state.name,
          params: params
        });

        $state.go('login');
      }
    }
  });

  $rootScope.$on('$stateChangeSuccess', function (event, state, params) {
    // Clear pending state if navigated to a non-authentication related section
    if (['login', 'register'].indexOf(state.name) === -1) {
      authManager.setRequestedStateData(null);
    }
  });
});
