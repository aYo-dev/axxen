'use strict';

angular.module('axxen').directive('axCalculator', function (placeProxyService, insuranceProxyService) {

  return {
    restrict: 'E',
    replace: true,
    link: function ($scope, $element, $attrs) {
      $scope.insuranceData = {
        vehicles: ['car','bus']
      };

      $scope.matches = [];
      $scope.error = null;

      $scope.$watch(function() {
        return $scope.insuranceData.vehicleType;
      }, function(newValue, oldValue){
        if(!_.isEmpty(newValue)){
          _getOptions(newValue);
        }
      });

      $scope.getMatchesAndCalculatePrice = function() {
        // console.log($scope.insuranceData);
        insuranceProxyService.getMatchesAndCalculatePrice($scope.insuranceData).then(function(result) {
          $scope.matches = result;
        }).catch(function(err) {
          console.error(err);
        });
      };

      function _getOptions(vehicleType) {
        insuranceProxyService.getOptionsByVehicleType(vehicleType).then(function(options){
          $scope.options = _.map(options, function(option) {
            return option.name;
          });
        }).catch(function(err) {
          console.error(err);
        });
      }
    },
    scope: {},
    templateUrl: 'views/directives/ax-calculator.html'
  };
});
