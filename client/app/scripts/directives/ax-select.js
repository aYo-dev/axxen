'use strict';

angular.module('axxen').directive('axSelect', function () {

  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    require: 'ngModel',
    link: function ($scope, $element, $attrs, $ngModel, $transcludeFn) {
      if (!$ngModel) {
        return
      };

      $element.append($transcludeFn());

      $scope.selectItem = function (item) {
        $ngModel.$setViewValue(item);
      };
    },
    templateUrl: 'views/directives/ax-select.html',
    scope: {
      items: '='
    }
  };
});
