'use strict';

angular.module('axxen').directive('axMap', function () {

  return {
    restrict: 'E',
    replace: true,
    link: function ($scope, $element, $attrs) {
    	var cities = [
  	    {
	        city : 'Office 1',
	        desc : 'Primary office',
	        lat : 41.392208,
	        long : 23.207898
  	    },
  	    {
	        city : 'Office 2',
	        desc : 'Second Office',
	        lat : 41.399806,
	        long : 23.208198
  	    }
    	];

      var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(41.3965, 23.2100),
        mapTypeId: google.maps.MapTypeId.TERRAIN
      };

      $scope.map = new google.maps.Map($element.find('#map').get(0), mapOptions);

      $scope.markers = [];
      
      var infoWindow = new google.maps.InfoWindow();
      
      var createMarker = function (info){
        var marker = new google.maps.Marker({
          map: $scope.map,
          position: new google.maps.LatLng(info.lat, info.long),
          title: info.city
        });
        marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';
        
        google.maps.event.addListener(marker, 'click', function(){
          infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
          infoWindow.open($scope.map, marker);
        });
        
        $scope.markers.push(marker);
      }  
      
      for (var i = 0; i < cities.length; i++){
        createMarker(cities[i]);
      }

      $scope.openInfoWindow = function(e, selectedMarker){
        e.preventDefault();
        google.maps.event.trigger(selectedMarker, 'click');
      }
    },
    scope: {},
    templateUrl: 'views/directives/ax-map.html'
  };
});
