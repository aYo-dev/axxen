'use strict';

angular.module('axxen').directive('axNewOption', function () {

  return {
    restrict: 'E',
    replace: true,
    link: function ($scope, $element, $attrs) {
      $scope.removeOption = function() {
        $scope.options = [];
        $element.remove();
      };
    },
    scope: {
      options: '=',
      counter: '='
    },
    templateUrl: 'views/directives/ax-new-option.html'
  };
});
