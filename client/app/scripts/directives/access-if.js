'use strict';

angular.module('axxen').directive('obtAccessIf', function (ngIfDirective, accessExpressionContext) {
  var ngIf = ngIfDirective[0];

  return {
    restrict: 'A',
    transclude: 'element',
    priority: 650,
    terminal: true,
    $$tlb: true,
    link: function (scope, element, attrs, ctrl, transcludeFn) {
      var ngIfScope = scope.$new();
      angular.extend(ngIfScope, accessExpressionContext);

      attrs.ngIf = attrs.obtAccessIf;

      ngIf.link.call(ngIf, ngIfScope, element, attrs, ctrl, transcludeFn);
    }
  };
});
