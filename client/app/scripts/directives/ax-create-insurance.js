'use strict';

angular.module('axxen').directive('axCreateInsurance', function (insuranceProxyService, $compile) {

  return {
    restrict: 'E',
    replace: true,
    link: function ($scope, $element, $attrs) {
      $scope.optionsCounter = 0;
      $scope.error = null;
      $scope.newInsurance = {
        vehicleType: null,
        options: []
      };

      $scope.addOption = function() {
        if(_.isEmpty($scope.newInsurance.options)) {
          $scope.compileOptionDirective();
        }

        $scope.newInsurance.options.push(getOption());
      };

      $scope.compileOptionDirective =  function () {
        var inputTemplate = '<ax-new-option counter="optionsCounter" options="newInsurance.options"></ax-new-option>';
        var option = angular.element(inputTemplate);
        
        $compile(option)($scope);
        $element.find('#options-list').append(option);
        $scope.optionsCounter++;
      };

      $scope.removeOption = function(option) {
        $element.find('.option' + option).remove();
      };

      $scope.createInsurance = function () {
        insuranceProxyService.create($scope.newInsurance).then(function(result) {
          $scope.$emit('new insurance', result);
        }).catch(function(err) {
          $scope.error = err;
        });
      };

      function getOption() {
        return {
          prices:[
            {
              label: 'Зона 1'
            },
            {
              label: 'Зона 2'
            },
            {
              label: 'Зона 3'
            },
            {
              label: 'Под 25'
            },
            {
              label: 'Зелена карта'
            }
          ]
        };
      }
      
    },
    scope: {},
    templateUrl: 'views/directives/ax-create-insurance.html'
  };
});