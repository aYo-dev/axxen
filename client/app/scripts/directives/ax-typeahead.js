'use strict';

angular.module('axxen').directive('axTypeahead', function (
  placeProxyService,
  placeService) {

  return {
    restrict: 'E',
    replace: true,
    link: function ($scope, $element, $attrs) {
      var data = {};
      $scope.insuranceData.places = [];
      $scope.showPlacesList = false;
      console.log($scope.insuranceData);

      $scope.getPlaces = function(place) {
        if(!place || place.length === 0) {
          _resetItemsList();
          return;
        }

        data.placeName = place;

        placeProxyService.getPlaces(data).then(function(result) {
          if(!result) {
            return;
          }

          console.log(result);
          $scope.insuranceData.places = result.predictions;
          $scope.showPlacesList = result.predictions.length > 0;
        });
      };

      $scope.selectPlace = function(place) {
        $scope.insuranceData.city = place.description;
        _resetItemsList();
        _getPlaceState({placeId: place.place_id});
      };

      function _getPlaceState(placeId) {
        placeProxyService.getPlaceState(placeId).then(function(result) {
          if(!result){
            return;
          }

          $scope.insuranceData.state = placeService.findStateName(result);
          console.log('1',$scope.insuranceData.state);
        });
      }

      function _resetItemsList(){
        $scope.showPlacesList = false;
        $scope.insuranceData.places = [];
      }
    },
    scope: {
      insuranceData: '='
    },
    templateUrl: 'views/directives/ax-typeahead.html'
  };
});
