'use strict';

angular.module('axxen').directive('axCarousel', function () {

  return {
    restrict: 'E',
    replace: true,
    link: function ($scope, $element, $attrs) {
      $element.carousel({
        interval: 5000,
        pause: 'hover'
      });
    },
    scope: {},
    templateUrl: 'views/directives/ax-carousel.html'
  };
});
