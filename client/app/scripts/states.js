'use strict';

angular.module('axxen').config(function ($stateProvider) {
  $stateProvider.state('login', {
    url: '/login',
    templateUrl: 'views/login.html',
    controller: 'LoginController'
  }).state('main', {
    url: '/main',
    templateUrl: 'views/main.html',
    controller: 'MainController'
  }).state('admin', {
    url: '/admin',
    templateUrl: 'views/admin.html',
    controller: 'AdminController',
    resolve: {
      insurances: function (insuranceProxyService){
        return insuranceProxyService.get().then(function (insurances) {
          return insurances;
        }).catch(function(err){
          return err;
        });
      }
    }
  }).state('faq', {
    url: '/faq',
    templateUrl: 'views/faq.html',
    controller: 'FaqController'
  }).state('register', {
    url: '/register',
    templateUrl: 'views/register/step1.html',
    controller: 'RegisterUserController',
    data: {
      isAvailable: '!authenticated()'
    }
  }).state('change-password', {
    url: '/change-password',
    templateUrl: 'views/change-password.html',
    data: {
      isAvailable: 'authenticated()'
    }
  }).state('forgot-password', {
    url: '/forgot-password',
    templateUrl: 'views/forgot-password.html',
    data: {
      isAvailable: '!authenticated()'
    }
  }).state('unauthorized', { //by convention
    templateUrl: '/views/404.html'
  }).state('500', {
    url: '/500',
    templateUrl: 'views/500.html'
  }).state('not-allowed', {
    url: '/unauthorized',
    templateUrl: 'views/unauthorized.html'
  }).state('offline', {
    templateUrl: 'views/offline.html'
  });
});
