'use strict';

angular.module('axxen').controller('LoginController', function ($scope, $state, $rootScope, authManager) {
  $scope.username = '';
  $scope.password = '';
  $scope.authError = '';

  $scope.doLogin = function () {
    authManager.loginUser($scope.username, $scope.password).then(null, function (errorResponse) {
      if (errorResponse.code === 'PASSWORD_EXPIRED') {
        $rootScope.credentials = {
          username: $scope.username,
          password: $scope.password
        };
        $state.go('change-password', { e: true });
      } else {
        $scope.authError = errorResponse.reason;
      }
    });
  };

  $scope.forgotPassword = function () {
    $state.go('forgot-password');
  };

  $scope.handleKeyDown = function (event) {
    if (event.keyCode === 13) {
      $scope.doLogin();
    }
  };

  $scope.registerUser = function () {
    $state.go('register', null);
  };
});
