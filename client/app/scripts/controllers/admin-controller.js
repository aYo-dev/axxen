'use strict';

/**
 * @ngdoc function
 * @name axxen.controller:AdminController
 * @description
 * # AdminController
 * Controller of the axxen
 */

angular.module('axxen').controller('AdminController', function($scope, insuranceProxyService, insurances) {
  $scope.insurances = null;
  $scope.editableInsurance = null;
  $scope.error = {};

  _assignInsurancesToScope();

  $scope.makeEditable = function (insurance) {
    $scope.editableInsurance = !$scope.editableInsurance || $scope.isInsuranceChanged(insurance) ? angular.copy(insurance) : null;
  };

  $scope.removeInsurance = function (insuranceId) {
    insuranceProxyService.remove({insuranceId: insuranceId}).then(function(){
      _.remove($scope.insurances, function(el) {
        return el._id === insuranceId;
      });
    }).catch(function(err) {
      $scope.error.removeErr = err;
    });
  };

  $scope.editInsurance = function (editedInsurance) {
    var insurance = _findInsurance(editedInsurance._id);

    if($scope.isInsuranceChanged(insurance)) {
      insuranceProxyService.update(editedInsurance).then(function(result) {
        _updateInsurancesArr(editedInsurance);
        $scope.makeEditable(null);
      }).catch(function(err) {
        $scope.error.editErr = err.message;
      });
    }
  };

  $scope.isInsuranceChanged = function(insurance) {
    return !angular.equals($scope.editableInsurance, insurance);
  };

  $scope.$on('new insurance', function(event, newInsurance) {
    $scope.insurances.push(newInsurance);
  });

  function _assignInsurancesToScope() {
    if(!_.isArray(insurances)) {
      $scope.insurances = [];
      alert('Cannot get the data!');

      return;
    }

    $scope.insurances = insurances;
  }

  function _findInsurance(insuranceId) {
    return _.find($scope.insurances, function(el) {
      return el._id === insuranceId;
    });
  }

  function _updateInsurancesArr(insurance) {
    var index = _.indexOf($scope.insurances, _.find($scope.insurances, {_id: insurance._id}));
    $scope.insurances.splice(index, 1, insurance);
  }
});