'use strict';

/**
 * @ngdoc overview
 * @name axxen
 * @description
 * # axxen
 *
 * Main module of the application.
 */

angular.module('axxen', ['config', 'ngCookies', 'ngAnimate', 'ui.router', 'gettext', 'ui.bootstrap']).config(function ($httpProvider) {
  $httpProvider.defaults.withCredentials = true;

  $httpProvider.defaults.transformRequest.unshift(function (data, headersGetter) {
    if (headersGetter()['Content-Type'] === 'application/x-www-form-urlencoded') {
      return $.param(data);
    }

    return data;
  });

  $httpProvider.interceptors.push('authHttpResponseInterceptor');
}).config(function ($locationProvider, $urlRouterProvider) {
  $locationProvider.html5Mode(true);

  $urlRouterProvider.when('/', ['$state', 'authManager', function ($state, authManager) {
    var homeState = authManager.getHomeState();

    $state.go(homeState.name, homeState.params);
  }]).otherwise(function ($injector) {
    var $state = $injector.get('$state');

    $state.go('unauthorized');
  });
}).value('$uiViewScroll', angular.noop).run(function (gaService) {
  gaService.init();
}).run(['gettextCatalog', function (gettextCatalog) {
  gettextCatalog.currentLanguage = 'en';
  //gettextCatalog.debug = true;
}])

/** Patch fix: state has to be initialized for ui-router; https://github.com/angular-ui/ui-router/issues/679 **/
.run(function ($state) {});
