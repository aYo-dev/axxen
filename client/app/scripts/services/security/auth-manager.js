'use strict';

angular.module('axxen').factory('authManager', function ($rootScope, $cookies, $state, $q, APP_CONFIG, loginServiceProxy, registerServiceProxy) {

  var user;

  var reloadInProgress = false;
  var isAuthenticationStateSynced = false;

  var observerCallbacks = [];

  function registerObserverCallback(name, callback, notifyIfNull) {
    if (!_.find(observerCallbacks, { name: name })) {
      observerCallbacks.push({
        name: name,
        callback: callback,
        notifyIfNull: notifyIfNull
      });
    }
  }

  function notifyObservers() {
    angular.forEach(observerCallbacks, function (observer) {
      if (!!user || observer.notifyIfNull) {
        observer.callback();
      }
    });
  }

  function setUser(newUser) {
    var oldUser = user;
    user = newUser;

    //TODO: Handle dates in the user object
    //angular.forEach(user.values, function(value) {
    //  value.someDate = value.someDate && new Date(value.someDate);
    //});

    var userChanged = !angular.equals(oldUser, user) || !isAuthenticationStateSynced;

    isAuthenticationStateSynced = true;

    if (userChanged) {
      $rootScope.currentLoggedUser = user;
      notifyObservers();
    }

    if (!reloadInProgress) {
      doPostAuthenticationRedirect();
    }

    reloadInProgress = false;
  }

  function handleServiceError(response) {
    return $q.reject(response.data);
  }

  function updateUserResponse(user) {
    var newUser = null;
    if (!_.isEmpty(APP_CONFIG.serviceBaseUrl) && !_.isEmpty(user)) {
      newUser = user;
    }
    setUser(newUser);
  }

  function reloadAuthenticatedUser() {
    if (reloadInProgress) {
      return;
    }
    reloadInProgress = true;
    isAuthenticationStateSynced = false;

    return loginServiceProxy.getAuthenticatedUser().then(updateUserResponse, function () {
      setUser(null);
      isAuthenticationStateSynced = true;
    });
  }

  function doPostAuthenticationRedirect() {
    var targetState = getRequestedStateData() || getHomeState();
    $state.go(targetState.name, targetState.params);

    setRequestedStateData(null);
  }

  function setRequestedStateData(stateData) {
    var requestedStateData = stateData || null;
    $cookies.requestedStateData = angular.toJson(requestedStateData);
  }

  function getRequestedStateData() {
    return angular.fromJson($cookies.requestedStateData) || null;
  }

  //TODO: return correct login state depending on user roles
  function getHomeState() {
    if (!getIsUserLogged()) {
      return { name: 'main' };
    } else {
      return { name: 'not-allowed' };
    }
  }

  // TODO: make sure there is roles property in the user object, or refactor to the appropriate name
  function getIsUserLogged() {
    return !!(user && user.roles);
  }

  function hasAnyRole(roles) {
    if (!getIsUserLogged()) {
      return false;
    }

    return _.intersection(user.roles, roles).length > 0;
  }

  function hasAllRoles(roles) {
    if (!getIsUserLogged()) {
      return roles.length === 0;
    }

    return _.difference(roles, user.roles).length === 0;
  }

  function loginUser(username, password, rememberMe) {
    return loginServiceProxy.login(username, password, rememberMe).then(updateUserResponse, handleServiceError);
  }

  function loginWithToken(loginToken) {
    return loginServiceProxy.loginWithToken(loginToken).then(updateUserResponse, handleServiceError);
  }

  function registerUser(user) {
    return registerServiceProxy.registerUser(user).then(updateUserResponse, handleServiceError);
  }

  function logoutUser() {
    loginServiceProxy.logout();
    setUser(null);
  }

  return {
    registerObserverCallback: registerObserverCallback,

    setRequestedStateData: setRequestedStateData,
    getHomeState: getHomeState,

    loginUser: loginUser,
    loginWithToken: loginWithToken,
    registerUser: registerUser,
    logoutUser: logoutUser,
    reloadAuthenticatedUser: reloadAuthenticatedUser,
    isAuthenticationStateSynced: function () {
      return isAuthenticationStateSynced;
    },

    getIsUserLogged: getIsUserLogged,
    hasAnyRole: hasAnyRole,
    hasAllRoles: hasAllRoles
  };
});
