'use strict';

angular.module('axxen').factory('accessExpressionEvaluator', function ($parse, accessExpressionContext) {
  return function (expression) {
    return $parse(expression)(accessExpressionContext);
  };
});
