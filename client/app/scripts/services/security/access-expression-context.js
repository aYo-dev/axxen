'use strict';

angular.module('axxen').factory('accessExpressionContext', function (authManager) {
  return {
    hasRole: function (role) {
      return authManager.hasAnyRole([role]);
    },

    anyRole: function () {
      return authManager.hasAnyRole(arguments);
    },

    allRoles: function () {
      return authManager.hasAllRoles(arguments);
    },

    authenticated: function () {
      return authManager.getIsUserLogged();
    }
  };
});
