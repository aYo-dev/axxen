'use strict';

angular.module('axxen').factory('placeService', function (placeProxyService) {

  function findStateName(cityDetails) {
    var addressComponents = cityDetails.result.address_components;

    var stateDetails = _.find(addressComponents, function (el) {
      return _.includes(el.types, 'administrative_area_level_1');
    });

    return stateDetails.long_name;
  }

  return {
    findStateName: findStateName
  };
});
