'use strict';

angular.module('axxen').factory('gaService', function ($window, APP_CONFIG) {

	function init() {
		if (_.isEmpty(APP_CONFIG.googleAnalyticsKey)) {
			return;
		}

		/* jshint maxlen: false, sub: true, expr: true */

		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments);
			}, i[r].l = 1 * new Date();a = s.createElement(o), m = s.getElementsByTagName(o)[0];a.async = 1;a.src = g;m.parentNode.insertBefore(a, m);
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
		$window.ga('create', APP_CONFIG.googleAnalyticsKey, 'auto');
	}

	function trackPageView(page) {
		if (!$window.ga) {
			return;
		}

		$window.ga('set', 'page', page);
		$window.ga('send', 'pageview');
	}

	function trackEvent(category, action, label, value) {
		if (!$window.ga) {
			return;
		}
		if (value) {
			var parsed = parseInt(value, 10);
			value = isNaN(parsed) ? 0 : parsed;
		}
		$window.ga('send', 'event', category, action, label, value);
	}

	return {
		init: init,
		trackPageView: trackPageView,
		trackEvent: trackEvent
	};
});
