'use strict';

angular.module('axxen').factory('authHttpResponseInterceptor', function ($q, $location, $injector) {
  function redirectToLogin() {
    var authManager = $injector.get('authManager');
    var gaService = $injector.get('gaService');
    gaService.trackEvent('session', 'expire');
    authManager.logoutUser();
    var state = $injector.get('$state'); //This is needed because some bug with circular dependency of $state

    authManager.setRequestedStateData({
      name: state.current.name,
      params: state.params
    });

    state.go('login');
  }

  return {
    response: function (response) {
      if (response.status === 401) {
        response.data = { code: 'BAD_CREDENTIALS', reason: response.data };
        redirectToLogin();
        return response;
      }
      // Spring session expire handler
      if (response.status === 200 && response.data === 'This session has been expired (possibly due to multiple ' + 'concurrent logins being attempted as the same user).') {
        response.data = { code: 'BAD_CREDENTIALS', reason: response.data };
        redirectToLogin();
        return response;
      }
      if (response.status >= 500) {
        $injector.get('gaService').trackEvent('error', 'server', response.data, 500);
        $location.path('/500');
      }

      return response || $q.when(response);
    },
    responseError: function (rejection) {
      if (rejection.status === 401) {
        redirectToLogin();
      }
      if (rejection.status === 500) {
        $location.path('/500');
      }
      return $q.reject(rejection);
    }
  };
});
