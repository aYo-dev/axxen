'use strict';

angular.module('axxen').factory('loginServiceProxy', function (apiCall) {
  var formUrlencoded = {
    'Content-Type': 'application/x-www-form-urlencoded'
  };

  return {
    login: function (username, password, rememberMe) {
      return apiCall('login/login').post({
        username: username,
        password: password,
        rememberMe: rememberMe
      }, null, formUrlencoded);
    },

    loginWithToken: function (token) {
      return apiCall('login/token').post({ token: token }, null, formUrlencoded);
    },

    getAuthenticatedUser: function () {
      return apiCall('login/authenticated').get();
    },

    logout: function () {
      return apiCall('logout').post();
    }
  };
});
