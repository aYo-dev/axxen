'use strict';

angular.module('axxen').factory('registerServiceProxy', function (apiCall) {
  return {

    registerUser: function (registerRequest) {
      return apiCall('register/user').post(registerRequest);
    }
  };
});
