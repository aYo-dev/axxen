'use strict';

angular.module('axxen').factory('placeProxyService', function (apiCall) {
  function getPlaces(data) {
    return apiCall('getPlaces').post(data);
  }

  function getPlaceState(place) {
    return apiCall('getPlaceState').post(place);
  }

  return {
    getPlaces: getPlaces,
    getPlaceState: getPlaceState
  };
});
