'use strict';

angular.module('axxen').factory('insuranceProxyService', function (apiCall) {
  function get() {
    return apiCall('getInsurances').get();
  }

  function create(insurance) {
    return apiCall('createInsurance').post(insurance);
  }

  function update(insurance) {
    return apiCall('updateInsurance').patch(insurance);
  }

  function remove(insuranceId) {
    return apiCall('deleteInsurance').delete(insuranceId);
  }

  function getOptionsByVehicleType(vehicleType) {
    return apiCall('insurances/getOptions').post({vehicleType: vehicleType});
  }

  function getMatchesAndCalculatePrice(insuranceData) {
    return apiCall('insurances/calculate').post(insuranceData);
  }

  return {
    get: get,
    create: create,
    update: update,
    remove: remove,
    getOptionsByVehicleType: getOptionsByVehicleType,
    getMatchesAndCalculatePrice: getMatchesAndCalculatePrice
  };
});