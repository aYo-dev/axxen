'use strict';

GLOBAL._ = require('lodash')

let express = require('express'),
  env = process.env.NODE_ENV || 'development',
  config = require('./config/config.js')[env],
  port = config.port,
  app = express();

require('./config/express.js')(app, config);
require('./config/database.js')(config);
require('./config/routes.js')(app);

app.listen(port, '0.0.0.0', null, () => {
	console.log('Server is running on port ' + port + ' ' + env);
});
