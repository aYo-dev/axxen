'use strict';

let PlaceController = require('../api/controllers/PlaceController.js');
let InsuranceController = require('../api/controllers/InsuranceController.js');

module.exports = function (app) {
  app.post('/login', (req, res, next) => {
    let auth = passport.authenticate('local', (err, user) => {
      if(err) return next(err);

      if(!user) {
        return res.status(404).json({message: 'Sorry, we cannot find this user!'})
      };

      req.logIn('user', (err, user) => {
        if(err) return next(err);

        res.status(200).json(user);
      });
    });

    auth(req, res, next);
  });

  app.post('/getPlaces', PlaceController.getPlaces);
  app.post('/getPlaceState', PlaceController.getPlaceState);
  app.get('/getInsurances', InsuranceController.getInsurances);
  app.patch('/updateInsurance', InsuranceController.update);
  app.delete('/deleteInsurance', InsuranceController.remove);
  app.post('/createInsurance', InsuranceController.create);
  app.post('/insurances/getOptions', InsuranceController.getOptions);
  app.post('/insurances/calculate', InsuranceController.calculateInsurancesPrice);

  // app.get('*', (req, res) => res.json({message: 'hello from mongoose'}));
}
