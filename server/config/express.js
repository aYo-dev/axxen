'use strict';

let express = require('express'),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  session = require('express-session'),
  passport = require('passport'),
  cookieParser = require('cookie-parser');
  session = require('express-session');


module.exports = function(app, config) {
  app.set('view engine', 'ejs');
  app.set('views', config.rootPath + '/views');
  app.use(passport.initialize());
  app.use(bodyParser.json());
  app.use(passport.session());
  app.use(express.static(config.rootPath + '/public'));
  app.use(cors({credentials: true, origin: true}));
  app.use(session({ 
    secret: 'axxen88',
    name: 'axxen',
    resave: true,
    saveUninitialized: false,
    cookie: { maxAge: 60000 }
  }));
};
