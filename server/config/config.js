'use strict';

let path = require('path');

function getRootPath (rootPath) {
  return path.normalize(__dirname + rootPath);
}

module.exports = {
  production: {
    rootPath: getRootPath('/../'),
    db: 'mongodb://axxen:aKitanov!$**@ds033285.mongolab.com:33285/axxen',
    port: process.env.PORT || 3030
  },
  development: {
    rootPath: getRootPath('/../'),
    db: 'mongodb://localhost/insurance_calculator',
    port: process.env.PORT || 3030
  }
}
