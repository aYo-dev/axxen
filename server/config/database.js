'use strict';

let mongoose = require('mongoose'),
    passport = require('passport'),
    LocalPassport = require('passport-local');

function checkAuthorizationRespons(err, user, done) {
  if(err) {
    console.log('Error loading user: ' + err);
  }

  if(user) {
    return done(null, user);
  }

  return done(null, false);
}

module.exports = function (config) {
  mongoose.connect(config.db);
  let db = mongoose.connection;

  db.once('open', err => {
    if(err) {
      console.log('Database cannot be opened: ' + err);
      return;
    }   

    console.log('Database up and running...');
  });

  db.on('error', err => console.log('Database error' + err));

  let userSchema = mongoose.Schema({
    username: String,
    password: String
  });

  let User = mongoose.model('User', userSchema);

  User.find({}).exec((err, users) => {
    if(err) {
      console.log('Cannot find user: ' + err);
    }

    if(users.length === 0) {
      User.create({username: 'admin', password: 'admin1234'});
      console.log('User is added to database!');
    }
  });

  /* for authentication with token*/
  passport.use(new LocalPassport((username, password, done) => {
    User.findOne({username: username}).exec((err, user) => {
      return checkAuthorizationRespons(err, user, done);
    });
  }));

  passport.serializeUser((user, done) => {
    if(user) {
      delete user.password;

      return done(null, user);
    }
  });

  passport.deserializeUser((user, done) => {
    User.findOne({_id: user.id}).exec((err, user) => {
      return checkAuthorizationRespons(err, user, done);
    });
  });
};
