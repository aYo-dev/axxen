'use strict';

let mongoose = require('mongoose');

module.exports = function () {
  let insuranceSchema = mongoose.Schema({
    company: {type: String, required: true},
    vehicleType: {type: String, required: true},
    options: {type: Array, required: true}
  });

  let Insurance = mongoose.model('Insurance', insuranceSchema);

  return Insurance;
};