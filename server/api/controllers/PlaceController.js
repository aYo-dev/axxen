'use strict';

let googlePlaceService = require('../services/GooglePlaceService.js');

function getPlaces (req, res) {
  let type = req.body.type || 'cities';
  let name = req.body.placeName;

  googlePlaceService.getPlaces(name, type).then(result => {
    if(!result) {
      return res.status(404).json({message: 'place not found'});
    }

    return res.status(200).json(result);
  }).catch(err => res.status(400).json(err));
}

function getPlaceState (req, res) {
  let placeId = req.body.placeId;

  googlePlaceService.getPlaceState(placeId).then(result => {
    if(!result) {
      return res.status(404).json({message: 'place not found'});
    }

    return res.status(200).json(result);
  }).catch(err => res.status(400).json(err));
}

module.exports = {
  getPlaces: getPlaces,
  getPlaceState: getPlaceState
};
