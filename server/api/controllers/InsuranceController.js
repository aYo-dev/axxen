'use strict';

let InsuranceService = require('../services/InsuranceService.js');

function getInsurances(req, res) {
  InsuranceService.getInsurances().then(insurances => {
    return res.status(200).json(insurances);
  }).catch(err => res.status(400).json(err));
}

function remove(req, res) {
  let insuranceId = req.query.insuranceId;

  InsuranceService.remove(insuranceId).then((result) => {
    res.status(200).json(result);
  }).catch(err => res.status(400).json({
    message: 'Can not find insurance with id:' + insuranceId
  }));
}

function update(req, res) {
  let insurance = req.body;

  InsuranceService.update(insurance).then(insurance => {
    return res.status(200).json(insurance);
  }).catch(err => res.status(400).json(err));
}

function create(req, res) {
  let insurance = req.body;

  InsuranceService.create(insurance).then(insurance => {
    return res.status(200).json(insurance);
  }).catch(err => res.status(400).json(err));
}

function getOptions(req, res) {
  let vehicleType = req.body.vehicleType;

  InsuranceService.getInsurancesByVehicleType(vehicleType).then(result => {
    if(_.isEmpty(result)) {
      throw {message: 'wrong vehicle type'};
    }

    req.session.insurancesMatchList = result;
    let response = _.isEmpty(result[0].options) ? [] : result[0].options;
    
    return res.status(200).json(response);
  }).catch(err => res.status(404).json(err));
}

function calculateInsurancesPrice(req, res) {
  let query = req.body;
  let insurances = req.session.insurancesMatchList;

  if(query.vehicleType !== insurances[0].vehicleType) {
    return res.status(404).json('Bad request');
  }
  
  InsuranceService.calculateInsurancesPrice(query, insurances).then(function(result) {
    return res.status(200).json(result);
  });
}

module.exports = {
  getInsurances: getInsurances,
  remove: remove,
  update: update,
  create: create,
  getOptions: getOptions,
  calculateInsurancesPrice: calculateInsurancesPrice
};