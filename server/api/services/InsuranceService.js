'use strict';

let Insurance = require('../models/Insurance.js')();

const ZONES = [['София-град', 'Софийска област'], ['Пловдив', 'Варна', 'Бургас']];
const YOUNG_DRIVER_YEARS = 25;

function getInsurances() {
  return new Promise((resolve, reject) => {
    Insurance.find({}).then(insurances => {
      return resolve(insurances);
    }).catch(err => reject(err));
  });
}

function remove(insuranceId){
  return new Promise((resolve, reject) => {
    Insurance.remove({_id: insuranceId}).exec((err) => {
      if(err) return reject(err);

      return resolve({message: 'Insurance was deleted succesfully'});
    });    
  });
}

function update(insurance){
  return new Promise((resolve, reject) => {
    Insurance.update({_id: insurance._id}, insurance, null, (err, insurance) => {
      if(err) return reject(err);

      return resolve(insurance);
    });
  });
}

function create(insurance){
  return new Promise((resolve, reject) => {
    Insurance.create(insurance).then(insurance => {
      return resolve(insurance);
    }).catch(err => reject(err));
  });
}

function getInsurancesByVehicleType(vehicleType) {
  return new Promise((resolve, reject) => {
    Insurance.find({vehicleType: vehicleType}).then(insurances => {
      if(_.isEmpty(insurances)) {
        return reject();
      }

      return resolve(insurances);
    });
  });
}

function calculateInsurancesPrice(query, insurances) {
  return new Promise((resolve, reject) => {
    let result = _.map(insurances, (insurance) => {
      insurance.price = _calculatePrice(insurance, query);

      return insurance;
    });

    return resolve(result);
  });
}

function _calculatePrice(insurance, query) {
  let option = _getCurrentOption(insurance.options, query);
  let zone = _getCurrentZone(query.state); 
  let greenCard = query.greenCard ? _.last(option.prices).price : 0;
  let experiancePremium = query.ownerAge <= YOUNG_DRIVER_YEARS ? option.prices[3].price : 0;
  
  return option.prices[zone].price + greenCard + experiancePremium;
}

function _getCurrentOption (options, query) {
  return _.find(options, option => {
    return option.name === query.option;
  });
}

function _getCurrentZone(state) {
  let currentZone = 2;

  _.forEach(ZONES, (zone, key) => {
    if(_.indexOf(zone, state) > -1) {
      currentZone = key;
      return;
    }
  });

  return currentZone;
};

module.exports = {
  getInsurances: getInsurances,
  remove: remove,
  update: update,
  create: create,
  getInsurancesByVehicleType: getInsurancesByVehicleType,
  calculateInsurancesPrice: calculateInsurancesPrice
}