'use strict';

const https = require('https'),
  key = 'AIzaSyBUMHFnrkdjqW0shfrN7pKmnYsUi059Nas',
  host = 'maps.googleapis.com';

function getPlaces (place, type) {
  return new Promise((resolve, reject) => {
    let options = _getPlaceOptions(encodeURIComponent(place), type, key);

    https.get(options, response => {
      let str = '';
        response.on('data', chunk => str +=chunk);
        response.on('end', () => resolve(JSON.parse(str)));
    }).on('error', (e) => {
      return reject(e);
    });
  });
}

function getPlaceState(placeId) {
  return new Promise((resolve, reject) => {
    let options = _getPlaceStateOptions(placeId, key);

    https.get(options, response => {
      let str = '';
        response.on('data', chunk => str +=chunk);
        response.on('end', () => resolve(JSON.parse(str)));
    }).on('error', (e) => {
      return reject(e);
    });
  });
}

function _doRequest(options) {
  return https.get(options, response => {
    let str = '';
      response.on('data', chunk => str +=chunk);
      response.on('end', () => JSON.parse(str));
  }).on('error', (e) => {
    throw e;
  });
}

function _getPlaceStateOptions(placeId, key) {
  return {
    host: host,
    path: '/maps/api/place/details/json?placeid='+ placeId+'&components=country:bg&language=bg_BG&key=' + key,
    method: 'GET'
  };
}

function _getPlaceOptions(place, type, key) {
  return {
    host: host,
    path: '/maps/api/place/autocomplete/json?input=' + place +'&types=('+ type +')&components=country:bg&language=bg_BG&key=' + key,
    method: 'GET'
  };
}

module.exports = {
  getPlaces: getPlaces,
  getPlaceState: getPlaceState
};
